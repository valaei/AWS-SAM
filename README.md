# sam-app

In order to deploy this project you need Python3 and Pip3, also you might need to install AWS SAM.

## Deployment Steps

1. Setup your AWS credentials

2. make install

3. make unit-test

4. make deploy-bucket (For packaging and Lambda requirements)

5. make deploy-app (Deploys App stack. Use API Gateway base URL output to test APIs. e.g. https://abcd1234.execute-api.ap-southeast-2.amazonaws.com/Prod/articles)

6. make integration-test (Tests API Gateway integration with App)

7. make teardown-app (In case you want to teardown the stacks)

8. make teardown-bucket

## Potential Improvements

- Add comments and exception handling to APIs (Please test with valid data)

- CI/CD Workflow could be added

- Use of env vars or config file instead of few minor hard-coded vars like stack-name

- Add json Schema and OpenAPI definition. (Currently automatically handled by AWS SAM and APIGW)
