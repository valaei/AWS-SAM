import json
import pytest
import boto3
from . import test_data
from moto import mock_dynamodb2
from api import get_article, post_article, get_tag

TABLE_NAME = "Articles"

@pytest.fixture()
def apigw_event():
    """ Generates API GW Event"""

    return test_data.apigw_event

@pytest.fixture
def use_moto():
    @mock_dynamodb2
    def dynamodb_client():
        dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-2')

        dynamodb.create_table(
            TableName=TABLE_NAME,
            KeySchema=[
                {
                    'AttributeName': 'Date',
                    'KeyType': 'HASH'
                },
                {
                    'AttributeName': 'Id',
                    'KeyType': 'RANGE'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'Date',
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'Id',
                    'AttributeType': 'S'
                }
            ],
            BillingMode='PAY_PER_REQUEST'
        )
        
        table = dynamodb.Table("Articles")
        table.put_item(
            Item = test_data.article_record
        )
        table.put_item(
            Item = test_data.article_record_2
        ) 
        return dynamodb
    return dynamodb_client

@mock_dynamodb2
def test_post_article(use_moto, apigw_event):
    use_moto()
    apigw_event["body"] = json.dumps(test_data.article_record_expected)
    ret = post_article.lambda_handler(apigw_event, "")

    assert ret["statusCode"] == 200

@mock_dynamodb2
def test_get_article(use_moto, apigw_event):
    use_moto()
    apigw_event["pathParameters"] = {"id": "1"}
    apigw_event["httpMethod"] = "GET"
    ret = get_article.lambda_handler(apigw_event, "")
    data = json.loads(ret["body"])

    assert ret["statusCode"] == 200
    assert ret["body"] == json.dumps(test_data.article_record_expected)

@mock_dynamodb2
def test_get_tag(use_moto, apigw_event):
    use_moto()
    apigw_event["pathParameters"] = {"tagName": "Tag3", "date": "20160922"}
    apigw_event["httpMethod"] = "GET"
    ret = get_tag.lambda_handler(apigw_event, "")
    data = json.loads(ret["body"])

    assert ret["statusCode"] == 200
    assert ret["body"] == json.dumps(test_data.tag_info_expected)