import os
import boto3
import requests
import json

from unittest import TestCase

class TestApiGateway(TestCase):
    api_endpoint: str

    @classmethod
    def get_stack_name(cls) -> str:
        stack_name = os.environ.get("AWS_SAM_STACK_NAME")
        if not stack_name:
            raise Exception(
                "Cannot find env var AWS_SAM_STACK_NAME. \n"
                "Please setup this environment variable with the stack name where we are running integration tests."
            )

        return stack_name

    def setUp(self) -> None:
        """
        Based on the provided env variable AWS_SAM_STACK_NAME,
        here we use cloudformation API to find out what the API Gateway URL is
        """
        stack_name = TestApiGateway.get_stack_name()

        client = boto3.client("cloudformation")

        try:
            response = client.describe_stacks(StackName=stack_name)
        except Exception as e:
            raise Exception(
                f"Cannot find stack {stack_name}. \n" f'Please make sure stack with the name "{stack_name}" exists.'
            ) from e

        stacks = response["Stacks"]

        stack_outputs = stacks[0]["Outputs"]
        api_outputs = [output for output in stack_outputs if output["OutputKey"] == "ApiEndpointUrl"]
        self.assertTrue(api_outputs, f"Cannot find output ApiEndpointUrl in stack {stack_name}")

        self.api_endpoint = api_outputs[0]["OutputValue"]

    def test_api_gateway(self):
        """
        Call the API Gateway endpoint and check the response
        """
        article_endpoint = self.api_endpoint + "articles/"
        article_record = { 
            "id": "1",
            "title": "Test Title",
            "date": "2016-09-22",
            "body": "Test Text",
            "tags": ["Tag1", "Tag2", "Tag3"]
        }

        response = requests.post(article_endpoint, json=article_record)
        self.assertEqual(response.status_code, 200)

        response = requests.get(article_endpoint + "1")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), article_record)


