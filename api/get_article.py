import boto3
import json

from boto3.dynamodb.conditions import Attr


def lambda_handler(event, context):
    """Get Article Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Response: dict

    """
    request_id = event["pathParameters"]["id"]

    dynamodb = boto3.resource("dynamodb")
    
    table = dynamodb.Table("Articles")
    response = table.scan(
        FilterExpression=Attr("Id").eq(request_id)
    )
    
    article = {}
    if response["Items"]:
        response = response["Items"][0]
        article = {
            "id": response["Id"],
            "title": response["Title"],
            "date": response["Date"],
            "body": response["Body"],
            "tags": response["Tags"]
        }

    return {
        "statusCode": 200,
        "body": json.dumps(article),
    }
