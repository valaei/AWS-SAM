import boto3
import json


def lambda_handler(event, context):
    """Post Article Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Response: dict

    """

    request_body = json.loads(event["body"])

    dynamodb = boto3.resource("dynamodb")
    
    table = dynamodb.Table("Articles")
    response = table.put_item(
        Item = { 
            "Id": request_body["id"],
            "Title": request_body["title"],
            "Date": request_body["date"],
            "Body": request_body["body"],
            "Tags": request_body["tags"]
       }
    )   

    return {
        "statusCode": 200,
    }
