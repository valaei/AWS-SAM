import boto3
import json
from boto3.dynamodb.conditions import Attr, Key


def lambda_handler(event, context):
    """Get Tag Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Response: dict

    """

    request_tag_name = event["pathParameters"]["tagName"]
    request_date = event["pathParameters"]["date"]
    formatted_date = "-".join([request_date[:4], request_date[4:6], request_date[6:]])

    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table("Articles")
    
    response = table.query(
        KeyConditionExpression=Key("Date").eq(formatted_date),
        FilterExpression=Attr("Tags").contains(request_tag_name),
    )

    articles = []
    related_tags = []
    count = 0
    for article in response['Items']:
        articles.append(article["Id"])
        related_tags.extend(article["Tags"])
        count += 1
        
    related_tags = list(dict.fromkeys(related_tags))
    if request_tag_name in related_tags:
        related_tags.remove(request_tag_name)
    
    return {
        "statusCode": 200,
        "body": json.dumps({
            "tag": request_tag_name,
            "count": count,
            "articles": articles,
            "related_tags": related_tags
        }),
    }
