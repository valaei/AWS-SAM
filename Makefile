SHELL := /bin/bash

deploy-bucket:
	sam deploy --template sam/bucket/template.yaml

teardown-bucket:
	sam delete --config-file sam/bucket/samconfig.toml --no-prompts

deploy-app:
	sam deploy --template sam/app/template.yaml

teardown-app:
	sam delete --config-file sam/app/samconfig.toml --no-prompts

unit-test:
	python3 -m pytest tests/unit -v

integration-test:
	AWS_SAM_STACK_NAME=sam-app python3 -m pytest tests/integration -v

install:
	pip3 install -r requirements.txt